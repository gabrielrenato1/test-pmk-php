# PMK Test CRUD de Doadores

## Introdução

Esta aplicação em PHP em conjunto com Javascript, é parte do teste de admição do PMK - Digital Marketing. Consiste em um CRUD que manipula um usuário Doador e armazena seus dados.

Agradeço a PMK pela oportunidade.

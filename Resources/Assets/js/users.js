var tgdeveloper = {
  /*
    Verificação de bandeira de cartão de crédito, código adaptado para o projeto.
    Autor do código original: Tiago Silva
    URL: https://tgdeveloper.com/blog/verificar-bandeira-de-cartao-de-credito-em-javascript
  */
  getCardFlag: function() {
      var cardnumber = document.getElementById('cardNumber').value.replace(/[^0-9]+/g, '');
      var cards = {
          visa      : /^4[0-9]{12}(?:[0-9]{3})/,
          mastercard : /^5[1-5][0-9]{14}/,
          diners    : /^3(?:0[0-5]|[68][0-9])[0-9]{11}/,
          amex      : /^3[47][0-9]{13}/,
          discover  : /^6(?:011|5[0-9]{2})[0-9]{12}/,
          hipercard  : /^(606282\d{10}(\d{3})?)|(3841\d{15})/,
          elo        : /^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})/,
          jcb        : /^(?:2131|1800|35\d{3})\d{11}/,
          aura      : /^(5078\d{2})(\d{2})(\d{11})$/
      };

      for (var flag in cards) {
          if(cards[flag].test(cardnumber)) {
            console.log(flag);
            document.getElementById('cardFlag').value = flag;
          }
      }
      return false;
  }
}

var display = {
  //Funções para montrar e esconder o método de pagamento
  paymentMethod: function() {
      var credit = document.getElementById('credit').checked;

      if (credit){
        document.getElementById('creditForm').style.display = "";
        document.getElementById('debitForm').style.display = "none";
      }else{
        document.getElementById('creditForm').style.display = "none";
        document.getElementById('debitForm').style.display = "";
      }
  },
  updateUser: function(){

    if(document.getElementById('updateUser').style.display == "none"){
      document.getElementById('updateUser').style.display = '';
    }else{
      document.getElementById('updateUser').style.display = 'none';

    }
  }
}

<?php

  function store(){
    require_once 'Conn/connection.php';
    //Validação se algum campo do usuário é vazio
    foreach ($_POST['user'] as $key => $attribute) {
      if(strlen($attribute) == 0){
        session_start();
        $_SESSION['error'] = "Campo vazio nos dados do doador";
        header('Location: '.'../../index.php/doadores');
        die;
      }
    }
    //cadastro do usuário se o método de pagamento for crédito
    if ($_POST['user']['payment_method'] == 'Crédito') {
      //Validação se algum campo do cartão é vazio
      foreach ($_POST['card'] as $key => $attribute) {
        if(strlen($attribute) == 0){
          session_start();
          $_SESSION['error'] = "Campo vazio nos dados do cartão";
          header('Location: '.'../../index.php/doadores');
          die;
        }
      }
      $number = substr_replace($_POST['card']['card_number'], '******', 6, 6);


      $sql = "INSERT INTO cards (number, flag, donate_value, created_at, updated_at)
                VALUES ('".$number."','".$_POST['card']['card_flag']."','".$_POST['card']['credit_donate_value']."',
                '".date('Y-m-d H-i-s')."','".date('Y-m-d H-i-s')."')";

      $query = mysqli_query($conn, $sql);

      if($query) {
        $sql = "INSERT INTO users (name,email,cpf,phone,birth,address,payment_method,created_at,updated_at, card_id)
                  VALUES ('".$_POST['user']['name']."', '".$_POST['user']['email']."', '".$_POST['user']['cpf']."', '".$_POST['user']['phone']."',
                  '".$_POST['user']['birth']."','".$_POST['user']['address']."','".$_POST['user']['payment_method']."','".date('Y-m-d H-i-s')."',
                  '".date('Y-m-d H-i-s')."','".$conn->insert_id."')";

        $query = mysqli_query($conn, $sql);
      }else{
        session_start();
        $_SESSION['error'] = mysqli_error($conn);
        header('Location: '.'../../index.php/doadores');
      }
    }else{
      //Validação se algum campo da conta é vazio
      foreach ($_POST['account'] as $key => $attribute) {
        if(strlen($attribute) == 0){
          session_start();
          $_SESSION['error'] = "Campo vazio nos dados da conta";
          header('Location: '.'../../index.php/doadores');
          die;
        }
      }
      $sql = "INSERT INTO accounts (type,bank,agency,account_number,person_type,donate_value,period)
                VALUES ('".$_POST['account']['account_type']."', '".$_POST['account']['account_bank']."', '".$_POST['account']['account_agency']."', '".$_POST['account']['account_number']."',
                '".$_POST['account']['person_type']."','".$_POST['account']['debit_donate_value']."','".$_POST['account']['donate_period']."')";

      $query = mysqli_query($conn, $sql);

      if($query) {
        $sql = "INSERT INTO users (name,email,cpf,phone,birth,address,payment_method,created_at,updated_at, account_id)
        VALUES ('".$_POST['user']['name']."', '".$_POST['user']['email']."', '".$_POST['user']['cpf']."', '".$_POST['user']['phone']."',
          '".$_POST['user']['birth']."','".$_POST['user']['address']."','".$_POST['user']['payment_method']."','".date('Y-m-d H-i-s')."',
          '".date('Y-m-d H-i-s')."','".$conn->insert_id."')";

          $query = mysqli_query($conn, $sql);
      }else{
        session_start();
        $_SESSION['error'] = mysqli_error($conn);
        header('Location: '.'../../index.php/doadores');
      }
    }

    if($query) {
      header('Location: '.'../../index.php/doadores');
    }else{
      session_start();
      $_SESSION['error'] = mysqli_error($conn);
      header('Location: '.'../../index.php/doadores');
    }
  }

  function index(){
    require_once 'Conn/connection.php';

    $sql = "SELECT * FROM users WHERE deleted_at is null ORDER BY id DESC";
    $query = mysqli_query($conn, $sql);
    if($query) {
      return mysqli_fetch_all($query, MYSQLI_ASSOC);
    }
  }

  function show(){
    require_once 'Conn/connection.php';

    $sql = "SELECT * FROM users WHERE deleted_at is null AND id = " . $_GET['user_id'];
    $query = mysqli_query($conn, $sql);

    $user = mysqli_fetch_assoc($query);

    if ($user['payment_method'] == 'Débito') {
      $sql = "SELECT * FROM accounts WHERE id = " . $user['account_id'];
      $query = mysqli_query($conn, $sql);

      $donation = mysqli_fetch_assoc($query);
    }else{
      $sql = "SELECT * FROM cards WHERE id = " . $user['card_id'];
      $query = mysqli_query($conn, $sql);

      $donation = mysqli_fetch_assoc($query);
    }
    if($query) {
      return ["user" => $user, "donation" => $donation];
    }
  }

  function update(){
    require_once 'Conn/connection.php';

    foreach ($_POST as $key => $attribute) {
      if(strlen($attribute) == 0){
        session_start();
        $_SESSION['error'] = "Campo vazio";
        header('Location: '.'../../index.php/doador?user_id='.$_GET['user_id']);
      }
    }

    $sql = "UPDATE users SET name = '".$_POST['name']."', email = '".$_POST['email']."',
    cpf = '".$_POST['cpf']."', phone = '".$_POST['phone']."', birth = '".$_POST['birth']."',
    updated_at = '".date('Y-m-d H-i-s')."', address = '".$_POST['address']."'
    WHERE id = " . $_GET['user_id'];

    $query = mysqli_query($conn, $sql);

    if($query) {
      header('Location: '.'../../index.php/doador?user_id='.$_GET['user_id']);
    }

  }

  function delete(){
    require_once 'Conn/connection.php';
    $sql = "UPDATE users SET deleted_at = '".date('Y-m-d H-i-s')."' WHERE id = " . $_GET['user_id'];
    $query = mysqli_query($conn, $sql);
    if($query) {
      header('Location: '.'../../index.php/doadores');
    }
  }
?>

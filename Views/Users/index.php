<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    <title>Doadores</title>
    <script src="users.js"></script>
    <link rel="stylesheet" href="usersTemplate.css">
    <script></script>
  </head>
  <body>
    <section class="container p-5">
      <div class="mb-3">
        <h1 class="text-center">Doadores</h1>
        <?php
          session_start();
          echo (isset($_SESSION['error'])) ? '<div class="text-center"><span class="error"><b>Erro:</b> '.$_SESSION['error'].'</span></div>' : '';
          $_SESSION['error'] = null;
        ?>
      </div>

      <div class="">
        <!-- Form update User -->
        <form class="" action="doadores/create" method="post">
          <div class="mb-3">
            <input class="form-control" type="text" name="user[name]" value="" placeholder="Nome">
          </div>
          <div class="mb-3">
            <input class="form-control" type="text" name="user[email]" value="" placeholder="E-mail">
          </div>
          <div class="row mb-3">
            <div class="col ">
              <input class="form-control" type="number" name="user[cpf]" value="" placeholder="CPF">
            </div>
            <div class="col">
              <input class="form-control" type="text" name="user[phone]" value="" placeholder="Telefone">
            </div>
            <div class="col">
              <input class="form-control" type="date" name="user[birth]" value="" placeholder="Data de Aniversário">
            </div>
          </div>
          <div class="mb-3">
            <input class="form-control" type="text" name="user[address]" value="" placeholder="Endereço">
          </div>

          <div class="form-check d-flex justify-content-around mb-3">
            <div class="">
              <input class="form-check-input" type="radio" name="user[payment_method]" id="debit" value="Débito" onclick="display.paymentMethod()">
              <label for="debit">Débito</label>
            </div>
            <div class="">
              <input class="form-check-input" type="radio" name="user[payment_method]" id="credit" value="Crédito" onclick="display.paymentMethod()">
              <label for="credit">Crédito</label>
            </div>
          </div>

          <!-- Debit form -->
          <div class="mb-3" id="debitForm" style="display:none">
            <div class="mb-3">
              <select class="form-select" name="account[account_type]">
                <option value="null" disabled selected>Tipo de conta</option>
                <option value="conta corrente">Conta Corrente</option>
                <option value="poupança">Poupança</option>
              </select>
            </div>
            <div class="mb-3">
              <select class="form-select" name="account[person_type]">
                <option value="null" disabled selected>Tipo de Pessoa</option>
                <option value="juridica">Juridica</option>
                <option value="fisica">Física</option>
              </select>
            </div>
            <div class="mb-3">
             <input class="form-control" type="number" name="account[account_bank]" value="" placeholder="Código do banco">
            </div>
            <div class="mb-3">
             <input class="form-control" type="number" name="account[account_agency]" value="" placeholder="Agência">
            </div>
            <div class="mb-3">
             <input class="form-control" type="number" name="account[account_number]" value="" placeholder="Número da conta">
            </div>
            <div class="mb-3">
              <select class="form-select" name="account[donate_period]">
                <option value="null" disabled selected>Intervalo de Doação</option>
                <option value="unico">Único</option>
                <option value="bimestral">Bimestral</option>
                <option value="semestral">Semestral</option>
                <option value="anual">Anual</option>
              </select>
            </div>
            <div class="mb-3">
              <input class="form-control" type="number" name="account[debit_donate_value]" placeholder="Valor da doação">
            </div>
          </div>

          <!-- Credit form -->
          <div class="mb-3" id="creditForm" style="display:none">
            <div class="mb-3">
             <input class="form-control" type="number" name="card[credit_donate_value]" placeholder="Valor da doação">
            </div>
            <div class="mb-3">
              <input class="form-control" type="text" name="card[card_name]" value="" placeholder="Nome impresso no cartão">
            </div>
            <div class="mb-3 d-flex">
              <input class="form-control" type="text" name="card[card_number]" id="cardNumber" value="" placeholder="Número do cartão" onkeyup="tgdeveloper.getCardFlag()">
              <div class="">
                <input class="form-control" type="text" name="card[card_flag]" id="cardFlag" readonly value="" placeholder="Bandeira">
              </div>
            </div>
            <div class="d-flex justify-content-between">
              <div class="row">
                <div class="col">
                  <input class="form-control" min="1" max="12" type="number" name="card[card_expiration_mounth]" value="" placeholder="Mês de expiração">
                </div>
                <div class="col">
                  <input class="form-control" type="number" name="card[card_expiration_year]" value="" placeholder="Ano de expiração">
                </div>
              </div>
              <div class="">
                <input class="form-control" type="number" name="card[card_cvv]" value="" placeholder="CVV">
              </div>
            </div>
          </div>
          <div class="d-flex justify-content-center">
            <button class="btn btn-primary" type="submit">Cadastrar Doador</button>
          </div>
        </form>
      </div>
      <hr class="">
      <!-- list Users -->
      <div class="">
        <?php
          require_once 'Controllers/Users/UsersController.php';
          $users = index();
          if(count($users) > 0){
        ?>
        <div class="">
          <div class="d-flex justify-content-center">
            <div class="col-sm-1">
              #
            </div>
            <div class="col-sm-2">
              NOME
            </div>
            <div class="col-sm-2">
              CPF
            </div>
            <div class="col-sm-3">
              EMAIL
            </div>
            <div class="col-sm-2">
              CRIADO EM
            </div>
          </div>
        </div>

        <?php
          }
          foreach ($users as $key => $value) { ?>
          <div class="users">
            <a href="<?= 'doador?user_id=' . $value['id'] ?>">
              <div class="d-flex justify-content-center">
                <div class="col-sm-1">
                  <?= $value['id'] ?>
                </div>
                <div class="col-sm-2 user-list-attribute">
                  <?= $value['name'] ?>
                </div>
                <div class="col-sm-2">
                  <?= $value['cpf'] ?>
                </div>
                <div class="col-sm-3 user-list-attribute">
                  <?= $value['email'] ?>
                </div>
                <div class="col-sm-2">
                  <?= date('d/m/Y', strtotime($value['created_at'])) ?>
                </div>
              </div>
            </a>
          </div>
        <?php } ?>
      </div>
    </section>

  </body>
</html>

<?php
  require_once 'Controllers/Users/UsersController.php';
  $showReturn = show();
  $user = $showReturn['user'];
  $donate = $showReturn['donation'];
?>

<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    <script src="users.js"></script>
    <link rel="stylesheet" href="usersTemplate.css">
    <title>Doador</title>
  </head>
  <body>
    <section class="container p-5">
      <div class="mb-3">
        <h1 class="text-center">Doador</h1>
        <?php
          session_start();
          echo (isset($_SESSION['error'])) ? '<div class="text-center"><span class="error"><b>Erro:</b> um dos campos está vazio.</span></div>' : '';
          $_SESSION['error'] = null;
        ?>
      </div>

      <div class="row">
        <div class="col">
          <div class="mb-3">
            <div class="">
              <h3>Dados pessoais</h3>
            </div>
            <div class="">
              <div class="">
                <?= 'Nome: ' . $user['name']?>
              </div>
              <div class="">
                <?= 'Data de nascimento: ' . date('d/m/Y', strtotime($user['birth']))?>
              </div>
              <div class="">
                <?= 'CPF: ' . $user['cpf']?>
              </div>
              <div class="">
                <?= 'Endereço: ' . $user['address']?>
              </div>
              <div class="">
                <?= 'Criado em: ' . date('H:i d/m/Y', strtotime($user['created_at']))?>
              </div>
              <div class="">
                <?= 'Atualizado em: ' . date('H:i d/m/Y', strtotime($user['updated_at']))?>
              </div>
            </div>
          </div>
          <div class="">
            <div class="">
              <h3>Contato</h3>
            </div>
            <div class="">
              <div class="">
                <?= 'Email: ' . $user['email']?>
              </div>
              <div class="">
                <?= 'Telefone: ' . $user['phone']?>
              </div>
            </div>
          </div>
        </div>

        <div class="col">
         <div class="">
           <h3>Doação</h3>
         </div>
         <?php if($user['payment_method'] == 'Crédito'){ ?>
           <div class="">
             <div class="">
               <?= 'Método de pagamento: ' . $user['payment_method']?>
             </div>
             <div class="">
               <?= 'Intervalo de doação: ' . $donate['number']?>
             </div>
             <div class="">
               <?= 'Intervalo de doação: ' . $donate['flag']?>
             </div>
             <div class="">
               <?= 'Intervalo de doação: R$' . $donate['donate_value']?>
             </div>
           </div>

         <?php }else{ ?>
           <div class="">
             <div class="">
               <?= 'Método de pagamento: ' . $user['payment_method']?>
             </div>
             <div class="">
               <?= 'Código do banco: ' . $donate['bank']?>
             </div>
             <div class="">
               <?= 'Tipo de conta: ' . $donate['type']?>
             </div>
             <div class="">
               <?= 'Valor da doação: ' . $donate['donate_value']?>
             </div>
           </div>
       <?php } ?>
       </div>
      </div>

      <div class="d-flex justify-content-between mt-3">
        <div class="">
          <a class="" href="doadores"><button class="btn btn-primary" type="button" name="button">Voltar</button></a>
        </div>
        <div class="d-flex justify-content-end">
          <button class="btn btn-primary mx-3" type="button" name="button" onclick="display.updateUser()"> Atualizar Doador</button>
          <a href="<?= 'doador/delete?user_id=' . $user['id']?>"><button class="btn btn-danger" type="button" name="button"> Deletar Doador</button></a>
        </div>
      </div>
      <hr>
      <!-- Form update User -->
      <div class="" id="updateUser" style="display:none">
        <form class="" action="<?= 'doador/update?user_id=' . $user['id']?>" method="post">
          <div class="mb-3">
            <input class="form-control" type="text" name="name" value="<?= $user['name']  ?>" placeholder="Nome">
          </div>
          <div class="mb-3">
            <input class="form-control" type="text" name="email" value="<?= $user['email']  ?>" placeholder="E-mail">
          </div>
          <div class="row mb-3">
            <div class="col ">
              <input class="form-control" type="number" name="cpf" value="<?= $user['cpf']  ?>" placeholder="CPF">
            </div>
            <div class="col">
              <input class="form-control" type="text" name="phone" value="<?= $user['phone']  ?>" placeholder="Telefone">
            </div>
            <div class="col">
              <input class="form-control" type="date" name="birth" value="<?= date('Y-m-d', strtotime($user['birth']))?>" placeholder="Data de Aniversário">
            </div>
          </div>
          <div class="mb-3">
            <input class="form-control" type="text" name="address" value="<?= $user['address']  ?>" placeholder="Endereço">
          </div>
          <div class="d-flex justify-content-center">
            <button class="btn btn-primary" type="submit">Enviar</button>
          </div>
        </form>
      </div>
    </section>

  </body>
</html>

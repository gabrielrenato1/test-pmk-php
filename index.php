<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$url = $_SERVER['PATH_INFO'];
if ($url === null) {
  header('Location: '.'index.php/doadores');
}

switch ($url) {
  case '/doadores':
    require 'Views/Users/index.php';
    break;
  case '/users.js':
    require 'Resources/Assets/js/users.js';
    break;
  case '/usersTemplate.css':
    require 'Resources/Assets/css/usersTemplate.css';
    break;
  case '/doadores/create':
    require_once 'Controllers/Users/UsersController.php';
    store();
    break;
  case $url == '/doador' && isset($_GET['user_id']):
    require 'Views/Users-detail/index.php';
    break;
  case $url == '/doador/update' && isset($_GET['user_id']):
    require_once 'Controllers/Users/UsersController.php';
    update();
    break;
  case $url == '/doador/delete' && isset($_GET['user_id']):
    require_once 'Controllers/Users/UsersController.php';
    delete();
    break;
  default:
    echo json_encode(['error' => '404', 'message' => 'Page not found']);
    break;
}
?>
